//import '../imports/ui/body/body.js';

Template.home.rendered = function(){
    setTimeout(function(){
        //se pueden hacer llamadas desde aqui
    },1000)
}

Template.home.events({
    'click button' (e){
        console.log($('#search').val());
        FindIt($('#search').val());
    },
    'click .play' (e){
        $('audio').remove();
        var it = $(e.currentTarget);
        var referenceObject = it.parents('.result');
        var src = referenceObject.attr('play');
        var audio = ' <audio controls>\
                            <source src="'+src+'" type="audio/ogg">\
                            <source src="'+src+'" type="audio/mpeg">\
                            Your browser does not support the audio element.\
                        </audio> ';
        referenceObject.append(audio);
    }
});

function FindIt(query){
    if(!query){
        console.log('nada que buscar');
        $('.results').empty();
    }else{
        var res = '';
        Meteor.call("FindAudios", query, function(error, result) {
            if (!error) {  
                //console.log('i\'m close');
                //console.log(result);
                DisplayIt(result);
            }else{
                console.log('my fault');
                console.log(error);
            }
        });
        
    }
}

function DisplayIt(f){
    //console.log(f.length);
    console.log(f);
    $('.results').empty();
    $.each(f, function(index, value){
        //console.log(value.title);
        var result = '<div class="result" play="'+value.file+'">\
                        <h3>'+value.title+'</h3>\
                        <img src="'+value.imgMini+'">\
                        <a class="play" href="">Reproducir</a>\
                    </div>'
        $('.results').append(result);
    })
}

function PlayIt(){

}